#ifndef RUBIK_H
#define RUBIK_H

#include <stdio.h>
#include <limits.h>
#include <string.h>
#include "util.h"

/* define "sticker" colors */
/* reference colors for each solved face */
enum {
  STCK_NO_COLOR = -1, /* definition or sentinel value */
  STCK_RED,
  STCK_BLUE,
  STCK_YELLOW,
  STCK_GREEN,
  STCK_ORANGE,
  STCK_WHITE
};

typedef struct rubix       rubix;
typedef struct color_comb  color_comb;
typedef struct center_list center_list;
typedef struct center      center;
typedef struct side_list   side_list;
typedef struct side        side;
typedef struct corner_list corner_list;
typedef struct corner      corner;

extern rubix *ptr_rubix_generator();
extern void   destroy_rubix(rubix *);
extern void   print_rubix(rubix *);

struct rubix {

 /* by convention, the rubix cube is represented by looking at the top left corner
  * so it has a top and bottom face, a left front and back face and a right front and back face
  * each of the 3 types of pieces are represented as being on the front right face; all
  * corners are internally codified as top left, sides as top middle and center as front.
  * their coordinate however are represented as top bottom R front L front, R back R back
  * and a 1-4 for corners top left, top right bottom right and bottom left (clockwise) or
  * a 1-4 for sides top right bottom left. The center doesn't need further coordinate.  */
  corner_list *ptr_corner_list_head; /* global list of corners */
  side_list   *ptr_side_list_head;   /* global list of sides */
  center_list *ptr_center_list_head; /* global list of centers */

  corner_list *ptr_current_top_left_corner; /* anchor point of the rubikx configuration */

  mem_list    *ptr_ref_head; /* list of allocated pointers from rubix */

  int seed;                  /* for random initial state */
  int color_index;           /* number of colors */
  color_comb  *ptr_color_list;
};

struct color_comb
{
  color_comb *ptr_next; /* Sll (no container for color) */
  /* unique color element   */
  /* order is not important */
  int color_code;

  int color_nb; /* number of defined colors 1,2 or 3 */

  int color[3]; /* the colors */

  mem_list *ptr_ref;
};

struct center_list {

  center_list *ptr_center_next; /* keyword "list" is omitted for readability */
  center      *ptr_center;

  mem_list *ptr_ref;
};

struct center {

  /* by construction, all corners are represented as the front top left corner */
  /* other representations should be built with an operator (a function) */
  /* therefore, each corners have a front color, a top color and a side (left) color */
  /* all neighboors are <side> type one behind, (center top left face) */
  side_list *ptr_side_above;
  side_list *ptr_side_below;
  side_list *ptr_side_left;
  side_list *ptr_side_right;

  int color_code; /* enum value of the color of this center */

};

struct side_list {

  side_list *ptr_side_next; /* keyword "list" is omitted for readability */
  side      *ptr_side;

  mem_list *ptr_ref;
};

struct side {

  /* by construction, all corners are represented as the front top left corner */
  /* other representations should be built with an operator (a function) */
  /* therefore, each corners have a front color, a top color and a side (left) color */
  /* all neighboors are <side> type one behind, (center top left face) */
  corner_list *ptr_corner_left;
  corner_list *ptr_corner_right;

  center_list *ptr_center_behind;
  center_list *ptr_center_bottom;

  int color_code; /* enum value of the color pair of this corner */

};

struct corner_list {

  corner_list *ptr_corner_next; /* keyword "list" is omitted for readability */
  corner      *ptr_corner;

  mem_list *ptr_ref;
};

struct corner {

  /* by construction, all corners are represented as the front top left corner */
  /* other representations should be built with an operator (a function) */
  /* therefore, each corners have a front color, a top color and a side (left) color */
  /* all neighboors are <side> type one behind, (center top left face) */
  side_list *ptr_side_behind; /* if current corner is anchor point, this is the top (1) side of the front left face */
  side_list *ptr_side_below;  /* if current corner is anchor point, this is the left (2) side of the front left face
                                                                         or the right (4) side of the front right face */
  side_list *ptr_side_beside; /* if current corner is anchor point, this is the top (1) side of the front right face */

  int color_code; /* enum value of the color triplet of this corner */

};

#endif
