#include <getopt.h>

#include "rubix.h"

struct options_set {
  int   log_flag;         /* write log? */
};

static void rubix_create(struct options_set *);
static void print_usage(struct option *, const char *);
static void init_options(struct options_set *, int, char **);
static int  log_flag;

int main(int argc, char **argv);
int main (int argc, char **argv)
{

/* optional :
--log : output logs/verbose mode NOTE: errors are never silent and sent to stderr */
  struct options_set *ptr_opt_set;
  MALLOC_PTR(ptr_opt_set, sizeof(struct options_set), "options");

  ptr_opt_set->log_flag   = 0;

  /* this might seems like overkill but it's more readable and allow better extension of invocation POSIX options. */
  init_options(ptr_opt_set, argc, argv);

  rubix_create(ptr_opt_set);

  return 0;
}

static void print_usage(struct option *long_option, const char *prog_name)
{
  int opt_index;

  printf("Usage: %s",prog_name);

  /* framework came from another application I had fairly reusable */
  for ( opt_index = 0 ; opt_index < 2 ; opt_index++ ) {

    printf(" --%s",long_option[opt_index].name);

    if ( !long_option[opt_index].flag && long_option[opt_index].val ) {
      printf(" (-%c)",long_option[opt_index].val);
    }

    if ( long_option[opt_index].has_arg )
      printf(" <value>");
  }

  printf("\n");
}

static void init_options(struct options_set *ptr_opt_set, int argc, char **argv)
{
  int cmd_line_opt;
  static struct option long_options[] =
    {
      /* These options set a flag. */
      {"log", no_argument,       &log_flag, 1},
      {"help", no_argument, 0, 0},
      /* These options don't set a flag.
         We distinguish them by their indices. */
      /* NONE */
      {0, 0, 0, 0}
    };

  while (1)
    {
      /* getopt_long stores the option index here. */
      int option_index = 0;

      cmd_line_opt = getopt_long (argc, argv, "lh",
                       long_options, &option_index);

      /* Detect the end of the options. */
      if ( cmd_line_opt == -1 )
        break;

      ptr_opt_set->log_flag = log_flag;

      switch ( cmd_line_opt )
        {
        case 'l':
        /* fallthrough */
        case 0:
          /* If this option set a flag, do nothing else now. */
          if ( long_options[option_index].flag != 0 )
            break;

          if ( !strcmp(long_options[option_index].name,"help") ) {
            print_usage(long_options,argv[0]);
            exit(0);
          }

          /* NONE (code for parsing arguments from options goes below) */

          /* EXAMPLE:
          if ( !strcmp(long_options[option_index].name,"nodes") )
            ptr_opt_set->nodes = atoi(optarg); */

          break;

        case 'h':
          print_usage(long_options,argv[0]);
          exit(0);
          break;

        case '?':
          /* getopt_long already printed an error message. */
          print_usage(long_options,argv[0]);
          exit (1);
          break;

        default:
          abort ();
        }
    }

  /* other errors from parsing:
  if ( !obligaroty_condition ) {
    printf("error: --arg argument not optional:\n");
    print_usage(long_options,argv[0]);
    exit(1);
  }*/

}

static void rubix_create(struct options_set *ptr_opt_set)
{
  rubix  *ptr_rubix;  /* problem */

  /* Spawn a problem */
  ptr_rubix  = ptr_rubix_generator();

  print_rubix(ptr_rubix);

  /* free memory */
  destroy_rubix(ptr_rubix);

}
