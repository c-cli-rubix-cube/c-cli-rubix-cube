#include "rubix.h"

static rubix      *ptr_rubix_alloc();
static color_comb *ptr_color_comb_create(rubix *, int, int, int);

static int bool_color_is_valid(int, int, int);
static int bool_colors_match_a_code(color_comb *, int, int, int);
static int color_code_id(rubix *, int, int, int);
static int nb_colors(int , int , int );
static int bool_colors_are_different(int , int , int);
static int bool_colors_are_color_type(int , int , int , int);

/**********/
/* PUBLIC */
/**********/

extern rubix *ptr_rubix_generator()
{
  rubix       *ptr_rubix = ptr_rubix_alloc();

  /* start with a "solved" cube */
  printf("color code %i\n",color_code_id(ptr_rubix,STCK_RED,STCK_YELLOW,STCK_GREEN));
  printf("color code %i\n",color_code_id(ptr_rubix,STCK_GREEN,STCK_YELLOW,STCK_GREEN));

  /* build front face */

  return ptr_rubix;
}

extern void destroy_rubix(rubix *ptr_rubix)
{
  destroy_mem_ref(ptr_rubix->ptr_ref_head);
  free(ptr_rubix);
}

extern void print_rubix(rubix *ptr_rubix)
{

  printf("Hello world!\n");

}

/***********/
/* PRIVATE */
/***********/
/* return a unique ID that corresponds to triplets, pair or color of a piece */
static int color_code_id(rubix *ptr_rubix, int color1, int color2, int color3)
{
  color_comb *ptr_color;

  for ( ptr_color = ptr_rubix->ptr_color_list ; ptr_color ; ptr_color = ptr_color->ptr_next ) {
    if(bool_colors_match_a_code(ptr_color,color1,color2,color3))
      return ptr_color->color_code;
  }

  ptr_color = ptr_color_comb_create(ptr_rubix,color1,color2,color3);

  if(ptr_color)
    return ptr_color->color_code;
  else
    return STCK_NO_COLOR;
}

static int bool_colors_match_a_code(color_comb *ptr_color, int color1, int color2, int color3)
{
  /* true if a color set match the color_comb */
  int color_match  = 0;
  int color_nb_ref = ptr_color->color_nb;
  int color_nb     = 0;
  int i;

  if(!bool_color_is_valid(color1,color2,color3)) /* "new" color must be valid */
    return 0;

  color_nb = nb_colors(color1,color2,color3);

  if(color_nb != color_nb_ref) return 0; /* colors can't match */

 /* color must be different, so the number of match must
  * be equal to number of colors */
  for(i = 0 ; i < color_nb ; i++){
    if(ptr_color->color[i] != STCK_NO_COLOR
        && (ptr_color->color[i] == color1
            || ptr_color->color[i] == color2
            || ptr_color->color[i] == color3))
      ++color_match;
  }

  return (color_match == color_nb) ? 1 : 0;
}

static color_comb *ptr_color_comb_create(rubix *ptr_rubix, int color1, int color2, int color3)
{
  color_comb *ptr_color = NULL;

  /* no match from the list */
  if(bool_color_is_valid(color1,color2,color3)){
    MALLOC_PTR_GC(ptr_color,ptr_rubix->ptr_ref_head,sizeof(color_comb),"color type");

    ptr_color->ptr_next = NULL;
    ptr_color->color_code = ptr_rubix->color_index;
    ++ptr_rubix->color_index;

    ptr_color->color[0] = color1;
    ptr_color->color[1] = color2;
    ptr_color->color[2] = color3;
    ptr_color->color_nb = nb_colors(color1,color2,color3);

    Sll_PUSH(ptr_rubix->ptr_color_list,ptr_color,ptr_next);
  }

  return ptr_color;
}

static int bool_color_is_valid(int color1, int color2, int color3)
{
  int color_nb = nb_colors(color1,color2,color3);

  /* color rules validation function */
  /* at least one color must be defined */
  /* no two color may be identical */
  /* preexistence is NOT verified as it should be obeyed by construction
   * this function is called at creation so duplication should be impossible
   * because of the implementation of ptr_color_comb_create */
  /* in case of 2 and 3 colors, rules are as follows:
   * green solved face opposes blue
   * red solved face opposes orange
   * white solved face opposes yellow */

  if(!color_nb) return 0; /* at least one color */

  if(color_nb == 1) return 1; /* one color is always valid */

  /* colors must be different */
  if(!bool_colors_are_different(color1,color2,color3)) return 0;

  /* green and blue cannot be valid */
  if(bool_colors_are_color_type(color1,color2,color3,STCK_GREEN)
     && bool_colors_are_color_type(color1,color2,color3,STCK_BLUE))
    return 0;

  /* red and orange cannot be valid */
  if(bool_colors_are_color_type(color1,color2,color3,STCK_RED)
     && bool_colors_are_color_type(color1,color2,color3,STCK_ORANGE))
    return 0;

  /* white and yellow cannot be valid */
  if(bool_colors_are_color_type(color1,color2,color3,STCK_WHITE)
     && bool_colors_are_color_type(color1,color2,color3,STCK_YELLOW))
    return 0;

  return 1;
}

static int nb_colors(int color1, int color2, int color3)
{
  int color_nb = 0;

  if(color1 != STCK_NO_COLOR) ++color_nb;
  if(color2 != STCK_NO_COLOR) ++color_nb;
  if(color3 != STCK_NO_COLOR) ++color_nb;

  return color_nb;
}

static int bool_colors_are_different(int color1, int color2, int color3)
{
  int color_nb = nb_colors(color1,color2,color3);

  if(!color_nb) return 0;

  if(color_nb == 1) return 0;

  /* color may match only if more than one. if more than one no 2 can be NO_COLOR */
  if((color1 == color2
      || color2 == color3
      || color1 == color3))
    return 0; /* colors must be different if defined */

  return 1;
}

static int bool_colors_are_color_type(int color1, int color2, int color3, int color_type)
{
  if(color1 == color_type
     || color2 == color_type
     || color3 == color_type) return 1;

  return 0;
}

static rubix *ptr_rubix_alloc()
{
  rubix     *ptr_rubix = NULL;

  /* allocated rubix memory */
  MALLOC_PTR(ptr_rubix, sizeof(rubix), "rubix pointer");

  /* init */
  ptr_rubix->ptr_corner_list_head = NULL;
  ptr_rubix->ptr_side_list_head   = NULL;
  ptr_rubix->ptr_center_list_head = NULL;

  ptr_rubix->ptr_current_top_left_corner = NULL;

  /* mem */
  ptr_rubix->ptr_ref_head         = NULL;

  ptr_rubix->color_index          = 0;
  ptr_rubix->ptr_color_list       = NULL;

  return ptr_rubix;
}
