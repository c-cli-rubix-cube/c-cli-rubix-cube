all: rubix-repl

rubix-repl: main.o rubix.o util.o bin
	gcc -g lib/main.o lib/rubix.o lib/util.o -o bin/rubix-repl

main.o: src/main.c lib
	gcc -g -c src/main.c -o lib/main.o

rubix.o: src/rubix.c lib
	gcc -g -c src/rubix.c -o lib/rubix.o

util.o: src/util.c lib
	gcc -g -c src/util.c -o lib/util.o

lib:
	mkdir lib

bin:
	mkdir bin

clean:
	rm -rf lib bin
